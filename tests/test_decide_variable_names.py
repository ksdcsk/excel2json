from unittest import TestCase

from src.script import decide_variable_names


class TestDecide_variable_names(TestCase):
    def test_decide_variable_names(self):
        self.assertEqual(
            decide_variable_names(
                ['a', 'b', 'c'],
                lambda x: x
            ),
            ['a', 'b', 'c']
        )

        self.assertEqual(
            decide_variable_names(
                ['a', 'a', 'a'],
                lambda x: x
            ),
            ['a', 'a2', 'a3']
        )

        self.assertEqual(
            decide_variable_names(
                ['a', 'b', 'c'],
                lambda x: x + x
            ),
            ['aa', 'bb', 'cc']
        )
