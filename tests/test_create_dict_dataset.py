from unittest import TestCase

from src.script import create_dict_dataset


class TestCreate_dict_dataset(TestCase):
    def test_create_dict_dataset(self):
        self.assertCountEqual(
            create_dict_dataset(
                ['a', 'b', 'c'],
                [
                    [1, 2, 3],
                    ['d', 'e'],
                    ['2']
                ]
            ),
            [
                {'a': 1, 'b': 2, 'c': 3},
                {'a': 'd', 'b': 'e'},
                {'a': '2'}
            ]
        )

        with self.assertRaises(ValueError) as context:
            create_dict_dataset(
                ['a', 'b', 'c'],
                [
                    ['1', '2', '3'],
                    ['1', '2', '3', '4']
                ]
            )
