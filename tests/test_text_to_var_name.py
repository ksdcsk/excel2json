from unittest import TestCase

from src.script import text_to_var_name


class TestText_to_var_name(TestCase):
    def test_text_to_var_name(self):
        self.assertEqual(text_to_var_name("a b", "_"), "a_b")

        self.assertEqual(text_to_var_name("a b", "$"), "a$b")

        self.assertEqual(text_to_var_name("a b\nc", "_"), "a_b_c")

        self.assertEqual(text_to_var_name("a b  ", "_"), "a_b")

        self.assertEqual(text_to_var_name(""), "noname")

        self.assertEqual(text_to_var_name(None), "noname")
