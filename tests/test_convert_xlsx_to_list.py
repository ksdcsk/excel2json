from unittest import TestCase

from src.script import convert_xlsx_to_list
import openpyxl
import tempfile


class TestConvert_xlsx_to_list(TestCase):
    _tmp_file = None
    _data = None

    def setUp(self) -> None:
        self._tmp_file = tempfile.NamedTemporaryFile(suffix='.xlsx')

        self._data = self.generateDataset()
        wb = self.dataset2Xlsx(self._data)

        wb.save(self._tmp_file.name)

    def tearDown(self) -> None:
        self._tmp_file.close()

    def test_convert_xlsx_to_list(self):
        self.assertCountEqual(
            convert_xlsx_to_list(self._tmp_file.name),
            self._data
        )

    @staticmethod
    def generateDataset(width=10, height=10):
        result = []
        cell_num = 0

        for y in range(0, height):
            row = []

            for x in range(0, width):
                row.append(
                    'cell' + str(cell_num)
                )

                cell_num += 1

            result.append(row)

        return result

    @staticmethod
    def dataset2Xlsx(data) -> openpyxl.Workbook:
        wb = openpyxl.Workbook()
        wb_active_sheet = wb.active

        for row in data:
            wb_active_sheet.append(row)

        return wb