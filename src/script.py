#
#
#


import openpyxl
import json
import sys


def main():
    # Except 2 args: {Input XLSX file} and {Output js file}
    # Todo: use argparse.
    if len(sys.argv) < 3:
        raise ValueError("Input file and/or output file not supplied.")
    else:
        input_file, output_file = sys.argv[1], sys.argv[2]

    raw_data = convert_xlsx_to_list(input_file)
    var_names = decide_variable_names(raw_data[0], text_to_var_name)
    dataset = create_dict_dataset(var_names, raw_data[1:])

    formatted_data = {
        'var_names': var_names,
        'dataset': dataset
    }

    write_to_file(output_file, convert_dict_to_json(formatted_data))


def create_dict_dataset(var_names, data):
    result = []
    var_count = len(var_names)

    for row_data in data:
        if len(row_data) > var_count:
            raise ValueError("The length of the data exceeds the number of declared variables.")

        row_dict = {}
        for i in range(0, len(row_data)):
            row_dict[var_names[i]] = row_data[i]

        result.append(row_dict)

    return result


def convert_dict_to_json(data):
    return json.dumps(data)


def write_to_file(file_name, data):
    with open(file_name, "w+") as f:
        f.write(data)


def decide_variable_names(user_var_names, naming_function):
    unique_names = []
    names_found = {}

    for user_var_name in user_var_names:
        formatted_name = naming_function(user_var_name)

        if formatted_name in unique_names:
            names_found[formatted_name] += 1

            var_name = formatted_name + str(names_found[formatted_name])
        else:
            names_found[formatted_name] = 1

            var_name = formatted_name

        unique_names.append(var_name)

    return unique_names


def text_to_var_name(text, illegal_char_replace_by='_'):
    new_var_name = "" if text is None else text.strip()  # type: str

    if new_var_name == "":
        return "noname"
    else:
        return new_var_name.replace("\n", illegal_char_replace_by).replace(" ", illegal_char_replace_by)


def convert_xlsx_to_list(input_file):
    result = []

    wb = openpyxl.load_workbook(input_file)
    wb_active_sheet = wb.active

    for row in wb_active_sheet.rows:
        row_list = []

        for cell in row:
            row_list.append(cell.value)

        result.append(
            row_list
        )

    return result


if __name__ == '__main__':
    main()
